package api.rest;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;


import domain.model.PortforlioStatistics;
import domain.service.ValuationService;

@ApplicationScoped
@Path("/valuation")
public class ValuationRestService {

	@Inject
	ValuationService valuationService;


	@GET
	@Path("{currency}")
	@Produces(MediaType.APPLICATION_JSON)
	public PortforlioStatistics valuatePortfolio(@PathParam("currency") String currency) {
		return this.valuationService.valuatePortfolio(currency);
	}
	
}