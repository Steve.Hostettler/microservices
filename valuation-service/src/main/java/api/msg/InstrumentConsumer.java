package api.msg;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import org.eclipse.microprofile.reactive.messaging.Incoming;

import domain.model.Instrument;
import domain.service.InstrumentRepository;
import lombok.extern.java.Log;

@ApplicationScoped
@Log
public class InstrumentConsumer {
	
	@Inject
	InstrumentRepository repository;
	
	@Incoming("instruments")
	public void updateInstrument(Instrument instrument) {
		log.info("Consumer got following message : " + instrument);
		if (repository.get(instrument.getId()) == null) {
			repository.add(instrument);	
		} else {
			repository.update(instrument);
		}
	}
}
