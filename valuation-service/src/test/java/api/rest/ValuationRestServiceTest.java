package api.rest;

import static io.restassured.RestAssured.when;
import static org.hamcrest.Matchers.containsString;

import org.junit.jupiter.api.Test;

import io.quarkus.test.junit.QuarkusTest;

@QuarkusTest
class ValuationRestServiceTest {


	@Test
	void testGetUSD() {
		when().get("/valuation/USD").then().body(containsString("currentValue"));
	}

	@Test
	void testGetEUR() {
		when().get("/valuation/EUR").then().body(containsString("currentValue"));
	}

}