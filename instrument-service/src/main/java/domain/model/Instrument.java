package domain.model;

import static com.fasterxml.jackson.annotation.JsonTypeInfo.Id.NAME;

import java.math.BigDecimal;
import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.SequenceGenerator;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeInfo.As;
import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Data;

@Data
@Entity
@DiscriminatorColumn(name = "instrumentType")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)


@JsonTypeInfo(use=NAME, include=As.PROPERTY, property="instrumentType1111")
@JsonSubTypes(value = { 
        @JsonSubTypes.Type(value = Bond.class, name = "B"),
        @JsonSubTypes.Type(value = Loan.class, name = "L"),
        @JsonSubTypes.Type(value = Deposit.class, name = "D")
    })
public abstract class Instrument {

	@Id
	@SequenceGenerator(name = "INSTRUMENT_SEQ", sequenceName = "INSTRUMENT_SEQ")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "INSTRUMENT_SEQ")
	private Long id;

	@NotNull
	private String brokerLei;

	@NotNull
	private String counterpartyLei;

	@NotNull
	private String originalCurrency;

	@NotNull
	private BigDecimal amountInOriginalCurrency;

	@NotNull
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
	private LocalDate dealDate;

	@NotNull
	private LocalDate valueDate;

	@NotNull
	@Column(name = "instrumentType", insertable = false, updatable = false)
	private String instrumentType;

}
