package api.rest;

import java.net.URI;
import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;

import org.eclipse.microprofile.reactive.messaging.Channel;
import org.eclipse.microprofile.reactive.messaging.Emitter;

import domain.model.Instrument;
import domain.service.InstrumentService;

@ApplicationScoped
@Path("/instrument")
public class InstrumentRestService {

	@Inject
	InstrumentService instrumentService;

	@Inject
	@Channel("instruments")
	Emitter<Instrument> instrumentEmitter;

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<Instrument> getAll() {
		return instrumentService.getAll();
	}

	@GET
	@Path("/count")
	@Produces(MediaType.APPLICATION_JSON)
	public Long count() {
		return instrumentService.count();
	}

	@GET
	@Path("{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Instrument get(@PathParam("id") Long instrumentId) {
		return instrumentService.get(instrumentId);
	}

	@PUT
	@Produces(MediaType.APPLICATION_JSON)
	public Response update(Instrument instrument) {
		instrumentService.update(instrument);
		instrumentEmitter.send(instrument);
		return Response.ok().build();
	}

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response create(Instrument instrument) {
		instrumentService.create(instrument);
		instrumentEmitter.send(instrument);

        URI resourceUri = UriBuilder.fromResource(InstrumentRestService.class).path("{id}").build(instrument.getId());
        
		return Response.created(resourceUri).entity(instrument.getId()).build();

	}

	@POST
	@Path("propagateAllInstruments")
	public Response propagateAllInstruments() {
		for (Instrument instrument : instrumentService.getAll()) {
			instrumentEmitter.send(instrument);
		}
		return Response.ok().build();
	}
}