package api.msg;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import org.eclipse.microprofile.reactive.messaging.Channel;
import org.eclipse.microprofile.reactive.messaging.Emitter;
import org.eclipse.microprofile.reactive.messaging.Incoming;

import domain.model.Instrument;
import domain.service.InstrumentService;
import lombok.extern.java.Log;

@ApplicationScoped
@Log
public class InstrumentConsumer {

	@Inject
	InstrumentService instrumentService;
	
    @Inject
    @Channel("instruments")
    Emitter<Instrument> instrumentEmitter;

	@Incoming("instrumentsUpdateReq")
	public void updateInstrument(final String message) {
		log.info("Consumer got following message : " + message);
		if ("all".equals(message)) {
			log.info("Send the current state of ALL instruments to the topic");
			for (Instrument instrument : instrumentService.getAll()) {
				instrumentEmitter.send(instrument);
			}
		} else {
			// interpret the instrument id
			try {
				Long instrumentId = Long.valueOf(message);
				log.info("Send the state of an instrument to the topic with id " + instrumentId);
				Instrument instrument = instrumentService.get(instrumentId);
				if (instrument != null) {
					instrumentEmitter.send(instrument);
				}
			} catch (NumberFormatException e) {
				throw new IllegalArgumentException("Message must be wither a numeric instrument identifier or 'all'");
			}
		}
	}
}
