package api.msg;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import javax.enterprise.inject.Any;
import javax.inject.Inject;

import org.junit.jupiter.api.Test;

import domain.model.Instrument;
import io.quarkus.test.common.QuarkusTestResource;
import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.oidc.server.OidcWiremockTestResource;
import io.smallrye.reactive.messaging.providers.connectors.InMemoryConnector;
import io.smallrye.reactive.messaging.providers.connectors.InMemorySink;
import io.smallrye.reactive.messaging.providers.connectors.InMemorySource;

@QuarkusTest
@QuarkusTestResource(KafkaTestResourceLifecycleManager.class)
@QuarkusTestResource(OidcWiremockTestResource.class)
class InstrumentConsumerTest {

	@Inject @Any
    InMemoryConnector connector; 
	
	@Test
	void testUpdateRegularInstrument() {
	       InMemorySource<String> instrumentsIn = connector.source("instrumentsUpdateReq");     
	       InMemorySink<Instrument> instrumentsOut = connector.sink("instruments");	       
	       instrumentsIn.send("42");
	       assertTrue(instrumentsOut.received().size() > 0);
	}
	
	@Test
	void testUpdateAllInstrument() {
	   InMemorySource<String> instrumentsIn = connector.source("instrumentsUpdateReq");     
	   InMemorySink<Instrument> instrumentsOut = connector.sink("instruments");	       
	   instrumentsIn.send("all");
	   assertEquals(10, instrumentsOut.received().size());

	}


}
