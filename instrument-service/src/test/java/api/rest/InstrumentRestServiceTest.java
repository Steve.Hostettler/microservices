package api.rest;

import static io.restassured.RestAssured.given;
import static io.restassured.RestAssured.when;
import static org.hamcrest.Matchers.containsString;

import java.math.BigDecimal;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.UUID;

import org.apache.http.HttpStatus;
import org.junit.jupiter.api.Test;

import domain.model.Bond;
import domain.model.Instrument;
import io.quarkus.test.junit.QuarkusTest;
import io.restassured.http.ContentType;

@QuarkusTest
class InstrumentRestServiceTest {


	@Test
	void testGetAll() {
		when().get("/instrument").then().body(containsString("254900LAW6SKNVPBBN21"));

	}

	@Test
	void testGet() {
		when().get("/instrument/1").then().body(containsString("254900LAW6SKNVPBBN21"));
	}

	@Test
	void testCount() {
		when().get("/instrument/count").then().body(containsString("11"));
	}
	
	@Test
	void create() {
		
        given().
        contentType(ContentType.JSON).
        body(getRandomInstrument()).
		when().post("/instrument").then().statusCode(HttpStatus.SC_CREATED);
	}
	
	@Test
	void update() {
		Instrument i = getRandomInstrument();
		i.setId(51l);
        given().
        contentType(ContentType.JSON).        
        body(i).
		when().put("/instrument").then().statusCode(HttpStatus.SC_OK);
	}

	
	@Test
	void propagateAllInstruments() {
		when().post("/instrument/propagateAllInstruments").then().statusCode(HttpStatus.SC_OK);
	}

	private Instrument getRandomInstrument() {
		GregorianCalendar calendar = new GregorianCalendar();
		calendar.setGregorianChange(new Date(Long.MIN_VALUE));
		LocalDate d = Instant.ofEpochMilli(calendar.getTime().getTime())
	      .atZone(ZoneId.systemDefault())
	      .toLocalDate();
	      
		Bond b = new Bond();
		b.setBrokerLei(UUID.randomUUID().toString());
		b.setCounterpartyLei(UUID.randomUUID().toString());
		b.setAmountInOriginalCurrency(new BigDecimal("0.0"));
		b.setOriginalCurrency("USD");
		b.setValueDate(d);
		b.setMaturityDate(d);
		b.setIsin(UUID.randomUUID().toString());
		b.setQuantity(Long.valueOf(Math.round(Math.random()*1000)));
		return b;
	}
}