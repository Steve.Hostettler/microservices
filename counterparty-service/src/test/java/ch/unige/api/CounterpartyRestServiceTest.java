package ch.unige.api;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.containsString;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import org.junit.jupiter.api.Test;

import io.quarkus.test.common.QuarkusTestResource;
import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.oidc.server.OidcWiremockTestResource;
import io.smallrye.jwt.build.Jwt;

@QuarkusTest
@QuarkusTestResource(OidcWiremockTestResource.class)
class CounterpartyRestServiceTest {


	@Test
	void testCounterpartyList() {
		given().auth().oauth2(getAccessToken("alice", new HashSet<>(Arrays.asList("user")))).when()
				.get("/counterparties").then().statusCode(200).body(containsString("984500C2EEUEB4A0C629"));
	}

	@Test
	void testCounterpartyCount() {
		given().when().get("/counterparties/count").then().statusCode(200).body(is("5"));
	}

	@Test
	void testGet() {
		given().when().get("/counterparties/984500C2EEUEB4A0C629").then().body(containsString("984500C2EEUEB4A0C629"));
	}

	private String getAccessToken(String userName, Set<String> groups) {
		return Jwt.preferredUserName(userName).groups(groups).issuer("https://server.example.com")
				.audience("https://service.example.com").sign();
	}
}
