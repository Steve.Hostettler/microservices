package ch.unige.service;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.transaction.HeuristicMixedException;
import javax.transaction.HeuristicRollbackException;
import javax.transaction.NotSupportedException;
import javax.transaction.RollbackException;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import org.junit.jupiter.api.Test;

import ch.unige.domain.model.Address;
import ch.unige.domain.model.Counterparty;
import ch.unige.domain.model.Registration;
import ch.unige.domain.model.STATUS;
import ch.unige.domain.service.CounterpartyService;

import io.quarkus.test.junit.QuarkusTest;

@QuarkusTest
class CounterpartyServiceImplTest {

	@Inject
	EntityManager em;
	@Inject
	UserTransaction transaction;

	@Inject
	CounterpartyService counterpartyService;

	@Test
	void testGetAll() {
		int size = initDataStore();
		assertEquals(size, counterpartyService.getAll().size());
	}

	@Test
	void testGet() {
		initDataStore();
		List<Counterparty> counterparties = counterpartyService.getAll();
		String lei = counterparties.get(0).getLei();
		Counterparty cpty = counterpartyService.get(lei);
		assertEquals(counterparties.get(0).getLei(), cpty.getLei());
		assertEquals(counterparties.get(0).getLegalAddress(), cpty.getLegalAddress());
	}

	@Test
	void testCount() {
		long size = initDataStore();
		long count = counterpartyService.count();
		assertEquals(size, count);
	}

	private List<Counterparty> getCounterparties() {

		List<Counterparty> counterparties = new ArrayList<>();
		long numberOfCpty = Math.round((Math.random() * 10)) + 5;
		for (int i = 0; i < numberOfCpty; i++) {
			counterparties.add(getRandomCounterparty());
		}
		return counterparties;

	}

	private int initDataStore() {
		int size = counterpartyService.getAll().size();
		List<Counterparty> counterparties = getCounterparties();
		try {
			transaction.begin();
			for (Counterparty c : counterparties) {

				em.persist(c);
			}
			transaction.commit();
		} catch (SystemException | RollbackException | HeuristicMixedException | HeuristicRollbackException
				| NotSupportedException e) {
			throw new java.lang.IllegalStateException(e);
		}
		return size + counterparties.size();
	}

	private Counterparty getRandomCounterparty() {
		Counterparty c = new Counterparty();
		c.setLei(UUID.randomUUID().toString());
		c.setName(UUID.randomUUID().toString());
		c.setStatus(STATUS.ACTIVE);

		Address a = new Address();
		a.setFirstAddressLine(UUID.randomUUID().toString());
		a.setCity(UUID.randomUUID().toString());
		a.setRegion(UUID.randomUUID().toString());
		a.setCountry(UUID.randomUUID().toString());
		a.setPostalCode(UUID.randomUUID().toString());

		Registration r = new Registration();
		r.setRegistrationAuthorityEntityID(UUID.randomUUID().toString());
		r.setRegistrationAuthorityID(UUID.randomUUID().toString());
		r.setJurisdiction(UUID.randomUUID().toString());

		c.setLegalAddress(a);
		c.setRegistration(r);

		return c;
	}
}
