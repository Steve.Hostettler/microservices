package ch.unige.api;

import java.util.List;

import javax.annotation.security.RolesAllowed;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.eclipse.microprofile.jwt.JsonWebToken;

import ch.unige.domain.model.Counterparty;
import ch.unige.domain.service.CounterpartyService;

@ApplicationScoped
@Path("/counterparties")
public class CounterpartyRestService {

	@Inject
	CounterpartyService counterpartyService;

	@Inject
	JsonWebToken jwt;

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@RolesAllowed("user")
	public List<Counterparty> getAll() {
		return counterpartyService.getAll();
	}

	@GET
	@Path("/count")
	@Produces(MediaType.APPLICATION_JSON)
	public Long count() {
		return counterpartyService.count();
	}

	@GET
	@Path("{lei}")
	@Produces(MediaType.APPLICATION_JSON)
	public Counterparty get(@PathParam("lei") String lei) {
		return counterpartyService.get(lei);
	}

}